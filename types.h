#ifndef SIM_TYPES_H
#define SIM_TYPES_H
/* $Id: types.h,v 1.1 2000/06/05 22:26:45 florea Exp $ */

#define NACHARS 128

typedef int bool;
typedef int ss_t[NACHARS][NACHARS];
typedef unsigned char uchar;

#endif
