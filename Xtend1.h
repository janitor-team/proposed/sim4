#ifndef XTEND1_H
#define XTEND1_H

/* $Id: Xtend1.h,v 1.3 1998/05/12 19:58:50 schwartz Exp $ */
extern int    Xextend_bw(uchar *,uchar *,int,int,int,int,int *,int *);
extern int    Xextend_fw(uchar *,uchar *,int,int,int,int,int *,int *);
#endif /* XTEND1_H */
